
### Generate previews from the PDFs
Run:

   ``genpreview DIR``

to generated HTML preview pages in DIR and all sub directories (first level).

If no PDFs exist yet, the available documents have to be converted to PDF first.
To do that, mount DIR locally with:

   ``sshfs <USER>@<HOST>://THE/DIR/ mnt/``

Then run for example:

   ``for ODT in $(find DIR -name "*.odt") ; do
       libreoffice --headless --convert-to pdf $ODT --outdir $(dirname $ODT) ;
     done``
